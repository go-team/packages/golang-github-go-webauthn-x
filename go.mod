module github.com/go-webauthn/x

go 1.23

toolchain go1.23.0

require golang.org/x/crypto v0.26.0
